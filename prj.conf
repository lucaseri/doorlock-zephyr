# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

CONFIG_GPIO=y

CONFIG_FLASH=y
CONFIG_FLASH_PAGE_LAYOUT=y
CONFIG_NVS=y
CONFIG_MPU_ALLOW_FLASH_WRITE=y

CONFIG_HEAP_MEM_POOL_SIZE=4096
CONFIG_ASSERT=y
CONFIG_EXCEPTIONS=y

# For debugging
CONFIG_SERIAL=y
CONFIG_USB_CDC_ACM=y
CONFIG_UART_LINE_CTRL=y
CONFIG_UART_CONSOLE=y

CONFIG_CONSOLE_SUBSYS=y
CONFIG_CONSOLE_GETLINE=y

CONFIG_CPLUSPLUS=y
CONFIG_LIB_CPLUSPLUS=y
CONFIG_STD_CPP17=y
CONFIG_NEWLIB_LIBC_NANO=n

CONFIG_NETWORKING=y
CONFIG_NET_SOCKETS=y
CONFIG_POSIX_API=y
CONFIG_NET_SOCKETS_POSIX_NAMES=y
CONFIG_NET_SOCKETPAIR=y

# Disable default configs
CONFIG_BT=n
CONFIG_NET_L2_ETHERNET=n
CONFIG_NET_L2_IEEE802154=n
CONFIG_NET_TCP=n
CONFIG_NET_IPV4=n
CONFIG_NET_DHCPV4=n
CONFIG_NET_IPV6_NBR_CACHE=n
CONFIG_NET_IPV6_MLD=y

# Kernel options
CONFIG_MAIN_STACK_SIZE=8192
CONFIG_SYSTEM_WORKQUEUE_STACK_SIZE=2048
CONFIG_ENTROPY_GENERATOR=y
CONFIG_INIT_STACKS=y
CONFIG_NEWLIB_LIBC=y

# Logging
CONFIG_NET_LOG=y
CONFIG_LOG=y
CONFIG_NET_STATISTICS=y
CONFIG_PRINTK=y

# Network buffers
CONFIG_NET_PKT_RX_COUNT=16
CONFIG_NET_PKT_TX_COUNT=16
CONFIG_NET_BUF_RX_COUNT=100
CONFIG_NET_BUF_TX_COUNT=100
CONFIG_NET_CONTEXT_NET_PKT_POOL=y

# IP address options
CONFIG_NET_IF_UNICAST_IPV6_ADDR_COUNT=5
CONFIG_NET_IF_MCAST_IPV6_ADDR_COUNT=10
CONFIG_NET_MAX_CONTEXTS=10
CONFIG_NET_CONFIG_SETTINGS=y
CONFIG_NET_CONFIG_NEED_IPV6=y
CONFIG_NET_CONFIG_MY_IPV6_ADDR="2001:db8:0:1::1"
CONFIG_NET_CONFIG_PEER_IPV6_ADDR="2001:db8:0:1::2"
CONFIG_NET_CONFIG_NEED_IPV4=n
CONFIG_NET_CONFIG_MY_IPV4_ADDR=""
CONFIG_NET_CONFIG_PEER_IPV4_ADDR=""

# TLS configuration
CONFIG_MBEDTLS=y
CONFIG_MBEDTLS_BUILTIN=y
CONFIG_MBEDTLS_ENABLE_HEAP=y
CONFIG_MBEDTLS_HEAP_SIZE=60000
CONFIG_MBEDTLS_SSL_MAX_CONTENT_LEN=2048

CONFIG_NET_SOCKETS_SOCKOPT_TLS=y
CONFIG_NET_SOCKETS_TLS_MAX_CONTEXTS=4
CONFIG_NET_SOCKETS_ENABLE_DTLS=y
CONFIG_POSIX_MAX_FDS=8
CONFIG_MBEDTLS_SSL_MAX_CONTENT_LEN=768

# Enable shell (including net and OpenThread)
CONFIG_SHELL=y
CONFIG_NET_SHELL=y
CONFIG_OPENTHREAD_SHELL=y
CONFIG_SHELL_STACK_SIZE=3072

# OpenThread support
CONFIG_NET_L2_OPENTHREAD=y
CONFIG_OPENTHREAD_DEBUG=y
CONFIG_OPENTHREAD_L2_DEBUG=y
CONFIG_OPENTHREAD_L2_LOG_LEVEL_INF=y
# Default values used on OTBR
# PAN ID 0x1357 to decimal 4951
CONFIG_OPENTHREAD_PANID=4951
CONFIG_OPENTHREAD_CHANNEL=26
CONFIG_OPENTHREAD_NETWORK_NAME="OniroThread"
CONFIG_OPENTHREAD_XPANID="0x11112222deadbeef"
CONFIG_OPENTHREAD_JOINER=y
CONFIG_OPENTHREAD_JOINER_AUTOSTART=y
CONFIG_OPENTHREAD_JOINER_PSKD="J01NU5"
CONFIG_OPENTHREAD_SLAAC=y
CONFIG_OPENTHREAD_THREAD_VERSION_1_2=y

CONFIG_NET_MGMT_EVENT_INFO=y
CONFIG_COAP=y
CONFIG_COAP_WELL_KNOWN_BLOCK_WISE=n
CONFIG_NET_UDP=y

CONFIG_LED_DOORLOCK=y
# CONFIG_LED_ROT_DOORLOCK=y
# CONFIG_SOLENOID_DOORLOCK=y
# CONFIG_ROTATING_DOORLOCK=y

CONFIG_KEYPAD_INPUT=y
CONFIG_SHELL_INPUT=y
CONFIG_COAP_INPUT=y
