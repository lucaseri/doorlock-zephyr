/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#ifdef CONFIG_SOLENOID_DOORLOCK

#ifndef SOLENOID_DOORLOCK_HPP
#define SOLENOID_DOORLOCK_HPP

#include "doorlock.hpp"

class solenoid_doorlock : public doorlock
{
public:
    solenoid_doorlock();
    virtual void unlock();
    virtual void lock();
private:
    const device *Pin0, *Pin1;
};

#endif //SOLENOID_DOORLOCK_HPP
#endif //CONFIG_SOLENOID_DOORLOCK
