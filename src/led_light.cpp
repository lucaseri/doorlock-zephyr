/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 */

#include "led_light.hpp"
#include "compat.h"
#include <zephyr.h>
#include <drivers/gpio.h>
#include <string.h>
#include <sys/printk.h>

#define LED0_NODE DT_ALIAS(led0)
#if DT_NODE_HAS_STATUS(LED0_NODE, okay)
static struct gpio_dt_spec led0 = GPIO_DT_SPEC_GET_OR(LED0_NODE, gpios, {0});
#else
#warning led0 not defined in devicetree, LED commands will be ignored
static struct gpio_dt_spec led0 = {NULL, 0, 0};
#endif




K_FIFO_DEFINE(led_blink_thread_control);

struct led_blink_thread_msg_t
{
    void *fifo_reserved;
     int frequency;
};

namespace {

void led_blink_thread_handler(void *__led_pointer, void *p2, void *p3)
{
    led_light * led_pointer = reinterpret_cast <led_light *> (__led_pointer);

    while (1) {
        led_blink_thread_msg_t *data = reinterpret_cast <led_blink_thread_msg_t *>
                                       (k_fifo_get(&led_blink_thread_control, K_FOREVER));
        int frequency = data->frequency;
        k_free(data);
        while (frequency) {
            led_pointer->led_toggle();
            data = reinterpret_cast <led_blink_thread_msg_t *>
                   (k_fifo_get(&led_blink_thread_control, K_MSEC(frequency)));
            if (data) {
                frequency = data->frequency;
                k_free(data);
            }
        }
        led_pointer->led_off();
    }
}

K_THREAD_STACK_DEFINE(led_stack_area, 512);

}  /* namespace */



led_light::led_light()
{
    this->__led_status = 0x00;

    void * __this = reinterpret_cast <void *> (this);
    
    led_tid = k_thread_create(&led_thread_data, led_stack_area,
                                 K_THREAD_STACK_SIZEOF(led_stack_area),
                                 led_blink_thread_handler,
                                 __this, NULL, NULL,
                                 7, 0, K_NO_WAIT);

    if (led0.port && !device_is_ready(led0.port)) {
        printk("LED led0 not ready, LED commands will be ignored\n");
        led0.port = NULL;
    }
    if (led0.port) {
      gpio_pin_configure_dt(&led0, GPIO_OUTPUT);
      gpio_pin_set_dt(&led0, 1);
    }
    else {
      throw(ENODEV);
    }
}

void led_light::led_on()
{
    gpio_pin_set_dt(&led0, 0);
    this->__led_status |= 1;
}

void led_light::led_off()
{
    gpio_pin_set_dt(&led0, 1);
    this->__led_status &= ~1;
}

void led_light::led_toggle()
{
    gpio_pin_set_dt(&led0, this->__led_status & 1);
    this->__led_status ^= 1;
}

void led_light::led_blink()
{
    printk("%d ", this->__led_status);
    this->led_toggle();
    k_msleep(500);
    this->led_toggle();
    k_msleep(500);
}

void led_light::led_start_blinking(int frequency)
{
    if (!frequency && !(this->__led_status & int(led_status::BLINKING)))
        return;
    
    struct led_blink_thread_msg_t msg = {.frequency = frequency};
    const size_t size = sizeof(struct led_blink_thread_msg_t);
    char *mem = reinterpret_cast <char *> (k_malloc(size));
    memcpy(mem, &msg, size);
    k_fifo_put(&led_blink_thread_control, mem);
    if (frequency)
        this->__led_status |= int(led_status::BLINKING);
    else
        this->__led_status &= ~int(led_status::BLINKING);
}

void led_light::led_stop_blinking()
{
    this->led_start_blinking(0);
}
