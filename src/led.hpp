/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 */

#pragma once

enum class led_status
{
	OFF = 0x00,
	ON = 0x01,
	BLINKING = 0x02
};

/**
 * @brief Initialize the LED
 */
void led_init();

/**
 * @brief Turn the LED on
 */
void led_on();

/**
 * @brief Turn the LED off
 */
void led_off();

/**
 * @brief Toggle the LED
 */
void led_toggle();

/**
 * @brief Blink the LED once (toggle for 1 second, then toggle back)
 */
void led_blink();

/**
 * @brief Start blinking the LED
 * @param frequency frequency in milliseconds
 */
void led_start_blinking(int frequency);

/**
 * @brief Stop blinking the LED
 */
void led_stop_blinking();

/**
 * @brief Get the status of the LED
 * @return Status of the LED
 */
int led_status();
