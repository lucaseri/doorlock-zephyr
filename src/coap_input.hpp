/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */
 
#ifndef COAP_INPUT_HPP

#ifdef CONFIG_COAP_INPUT

#include "input_device.hpp"
#include <string>

#define MAX_COAP_MSG_LEN 256
#define ALL_NODES_LOCAL_COAP_MCAST \
	{ { { 0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0xfd } } }

class coap_input : input_device {
public:

    coap_input(std::string port);

    long read_event() const;

    int fd() const noexcept;

private:
    int _m_sockfd;

    bool join_coap_multicast_group(uint16_t port);

    int start_coap_server(uint16_t port);

    void process_packet();
};

#endif // CONFIG_COAP_INPUT

#endif // COAP_INPUT_HPP
