/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */
 
#ifdef CONFIG_SHELL_INPUT

#include "input_device.hpp"
#include "shell_input.hpp"
#include <zephyr/net/socket.h>
#include <zephyr/shell/shell.h>

shell_input::shell_input() {
    int ret = zsock_socketpair(AF_UNIX, SOCK_STREAM, 0, this->_m_fd);
    if (ret != 0) {
      printk("Error %d while creating socket pair\n", errno);
      throw(errno);
    } 
    
    input_devices::add_input(this);
}

long shell_input::read_event() const {
    char key;
    int ret = read(this->_m_fd[1], &key, 1);
    if(ret < 0) {
        printk("Error while reading from file descriptor");
        throw(errno);
    }

    return (long) key;
}

int shell_input::fd() const noexcept {
    return this->_m_fd[1];
}

int shell_input::process(const shell *sh, int argc, char ** argv) {
    return write(this->_m_fd[0], argv[2], strlen(argv[2]));
}

shell_input shell_input;

namespace {
    int process_wrapper(const shell *sh, int argc, char **argv) {
        return shell_input.process(sh, argc, argv);
    }
}

SHELL_CMD_ARG_REGISTER(doorlock, NULL, "Pass input to doorlock. Usage: doorlock key #", process_wrapper, 3, 0);

#endif // CONFIG_SHELL_INPUT
