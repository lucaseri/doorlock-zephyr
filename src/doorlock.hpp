/* Copyright (C) 2021 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Bernhard Rosenkraenzer <bernhard.rosenkraenzer.ext@huawei.com>
 * Grzegorz Gwozdz <grzegorz.gwozdz@huawei.com>
 * Luca Seritan <luca.seritan@huawei.com>
 */

#ifndef DOORLOCK_HPP
#define DOORLOCK_HPP

#include <drivers/gpio.h>
#include <sys/printk.h>


/**
 * @brief Enum for state of lock
 * It tracks what is the current
 * state of the doorlock, it is useful
 * to tell whether there is a need
 * to start locking/unlocking
 * or it is irrelevant
 */
enum class locking_state 
{
    UNLOCKED = 1,
    LOCKED = 2
};

class doorlock
{
public:
    /**
     * @brief Construct a new doorlock object
     * 
     */
    doorlock();
    /**
     * @brief Unlock the doorlock
     */
    virtual void unlock();

    /**
     * @brief Locking the doorlocklock
     */
    virtual void lock();
    /**
     * @brief State of lock 
     * @return true if it is locked
     * false otherwise 
     */
    bool is_locked();
protected:
    locking_state _m_state; 
};
 
#endif
