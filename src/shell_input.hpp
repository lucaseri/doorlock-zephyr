/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */
 
#ifndef SHELL_INPUT_HPP
#define SHELL_INPUT_HPP

#ifdef CONFIG_SHELL_INPUT

#include "input_device.hpp"
#include <zephyr/net/socket.h>
#include <zephyr/posix/unistd.h>
#include <zephyr/shell/shell.h>

class shell_input : public input_device {
public:  

    shell_input();

    long read_event() const;

    int fd() const noexcept;

    int process(const shell *sh, int argc, char **argv);

private:
    int _m_fd[2];
};

#endif // CONFIG_SHELL_INPUT

#endif // SHELL_INPUT_HPP
