/* Copyright (C) 2022 Huawei Inc.
 * SPDX-FileCopyrightText: Huawei Inc.
 * SPDX-License-Identifier: Apache-2.0
 * Luca Seritan <luca.seritan@huawei.com>
 */
 
#ifndef INPUT_DEVICE_HPP
#define INPUT_DEVICE_HPP

class input_device {
public:
    virtual long read_event() const = 0;
    virtual int fd() const noexcept = 0;
};

namespace input_devices {
    void add_input(input_device *device);
};

#endif // INPUT_DEVICE_HPP
